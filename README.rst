DeepCyphers Valheim Dedicated Server
====================================

Cypherpunks code, but computer scientists also quite like to have fun and play together.

This repository is an easy-to-install helm-chart that is particularly focused on the security and maintainability of the code and containers. Many of the existing helm charts or docker containers are not as security focused.

Docker
------

The docker registry for linux/386, linux/amd64, linux/arm/v6, linux/arm/v7, linux/arm64/v8 is:

https://gitlab.com/deepcypher/dc-valheim/container_registry/2563249

Helm
----

Our helm charts are pushed to:

https://deepcypher.gitlab.io/dc-valheim/

Note there is no HTML so a browser will result in a 404.

To use the helm chart, add the repo:

```
helm repo add dc-valheim https://deepcypher.gitlab.io/dc-valheim
```

Ensure helm has the latest version of the repo:

```
helm repo update
```

Deploy the chart in the default cluster accessible by kubectl:

```
helm install valheim dc-valheim/valheim
```

To uninstall the chart again assuming the above is copy and pasted:

```
helm uninstall valheim
```
