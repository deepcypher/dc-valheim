#!/usr/bin/env bash

# @Author: archer
# @Date:   2021-12-12T09:53:30+00:00
# @Last modified by:   archer
# @Last modified time: 2021-12-12T10:43:55+00:00

ValPort="${VAL_PORT:-2456}"
ValName="${VAL_NAME:-Valhelm}"
ValPass="${VAL_PASS:-SuperSecretHelmPassword}"
ValSaveDir="${VAL_SAVE_DIR:-/app/save}"
ValWorld="${VAL_WORLD:-Dedicated}"
ValBinary="${VAL_BIN:-/app/valheim_server.x86_64}"

echo "name:${ValName}, port:${ValPort}, pass:${ValPass}, save_dir:${ValSaveDir}"

mkdir -p ${ValSaveDir}

export SteamAppId=892970
export LD_LIBRARY_PATH=/app/linux64:$LD_LIBRARY_PATH
${ValBinary} -name "${ValName}" -port "${ValPort}" -world "${ValWorld}" -password "${ValPass}" -savedir "${ValSaveDir}"
