# defining configurable args outside of FROM directive
# in case multi leve builds used in future
ARG appid="896660"
ARG appdir="/app"
ARG usr="archie"

# using official steamcmd alpine container for minimal speedup
FROM steamcmd/steamcmd
# taking our previously defined args and bringing them into this FROM directive
ARG appid
ARG appdir
ARG usr
USER root
# dropping privelages to a newly defined unprivelaged user
# by creating destination dir and setting user as owner pre-emtiveley
# RUN apk add --update shadow strace && \
RUN useradd -m $usr && \
    usermod --home /home/${usr} $usr && \
    mkdir -p ${appdir} && \
    apt update && apt install -y tree
# copy in our script to run the game but it will have wrong perms initially
COPY entrypoint.sh ${appdir}/.
RUN chown -R ${usr}:${usr} ${appdir}
USER $usr
# setting default directory to where our files will be
WORKDIR $appdir
# since it is a simple script we can generate an entrypoint.sh inside with all
# the arguments we desire pre-baked
RUN echo "HOME=/home/${usr} steamcmd +force_install_dir ${appdir} +login anonymous +app_update ${appid} +quit" > update.sh && \
    chmod 744 ${appdir}/update.sh && \
    ${appdir}/update.sh && \
    uname -a && \
    uname -m && \
    tree
# Document what ports we are interested in by default
EXPOSE 2456/udp
EXPOSE 2457/udp
EXPOSE 2458/udp
# get rid of any parent image entrypoint
ENTRYPOINT []
CMD ["/bin/bash", "-c", "/app/update.sh && /app/entrypoint.sh"]
